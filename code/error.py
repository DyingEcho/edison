#!/usr/bin/python
from output import say

def handle(error, occurence):
	say("Aw, snap!")
	say("It looks like I encountered an error.")
	say("Try filing an issue on my GitHub page. Mention error code " + error + " and include that it occured at " + occurence + ".")
	say("That's all I can tell you :( )")
	say("If you're running from the source code, look up your error code in errorcodes.txt for ways to prevent this.")
