#Edison SI by InfiniteJetpack © 2016
import random
import sys
import os
import time
import assets
import mood
import hashlib
from output import *
import definitions
from responses import *

'''
Password check created by a very helpful person on the RPi forums.
Password format in file is like this:

	matt,99fb2f48c6af4761f904fc85f95eb56190e5d40b1f44ec3a9c1fa319
	bob,78d8045d684abd2eece923758f3cd781489df3a48e1278982466017f

so, it's USERNAME,PASSWORD_HASH

Note - if opening password file, ensure you are using a text editor that does not automatically add newlines at the end of docs - this can break the program D:
'''

def respond(wordList):
	output = ""
	for word in wordList:
		output = (output + " " + (random.choice(word)))
	return output

def edison():
	mood = ask("Hi, " + username + "! How are you today? ")
	if mood.lower() in definitions.positive:
		print(respond([i_am, happy, to, hear, that]) + "!")
	elif mood.lower() in definitions.negative:
		print(respond([i_am, sorry_unhappy, to, hear, that]) + "!")
	sys.exit()

register = ask("Are you a new user? Please type Y or N. ")

password_file = '../assets/passwords.txt'
if register.lower() == "y":
    newusername = ask("What do you want your new username to be? ")
    newpassword = ask("What do you want your new password to be? ")


    username = (newusername)
    newpassword = hashlib.sha224(newpassword.encode('utf8')).hexdigest()

    file = open(password_file, "a")
    file.write("\n{0!s},{1!s}".format(newusername, newpassword))
    file.close()

    os.system("clear")
    edison()

elif register.lower() == ("n"):
    username = ask("Welcome! Could you please give me your username to log in? ")
    password = ask("Hi, " + username + "! What's your password? ")

    password = hashlib.sha224(password.encode('utf8')).hexdigest()


    say ("One second.")
    success = False
    with open(password_file) as f:
        for line in f:
            real_username, real_password = line.strip('\n').split(',')
            if username == real_username and password == real_password:
                success = True
                say("Login successful!")
                os.system("clear")
                edison()
    if success == False:
        say("I'm sorry, it seems that one or more of your login details were incorrect. Please try again.")
        say("If you keep getting this error, try submitting an issue on my GitHub page.")
