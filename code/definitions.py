negative = ["bad", "not good", "abominable", "atrocious", "awful", "deplorable", "no good", "distressed", "dreadful", "horrid", "ill", "sick", "lousy", "pain", "poor", "rotten", "sad", "terrible", "upset", "gloomy"]
positive = ["good", "amazing", "perfect", "wonderful", "fantastic", "satisfactory", "peachy", "not bad", "smashing", "quality", "best", "superb", "better", "dandy", "great"]
neutral = ["OK", "all right", "ok", "o.k", "alright", "a-okay"]
