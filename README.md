<img src="logo/logo_full.png" width="100%"/> 

# What do *you* want to do today?

[![Code Issues](https://www.quantifiedcode.com/api/v1/project/5d4f76fe856640ff8bfe7884a7a6d1d0/badge.svg)](https://www.quantifiedcode.com/app/project/5d4f76fe856640ff8bfe7884a7a6d1d0)



# Who, or what, is edison?

edison is a chatbot and PA, meant to make your life easier and more fun.

## Why should I download edison?

*   A natural-feeling experience
*   Constant development and imporvement
*   Issue tracking
*   More features on the way!

## This sounds great. How do I get started?

Luckily, it's simple! Just click the green button in the top-right corner, select 'Download ZIP', and you're on your way!

## Does edison have a webpage?
Yes! Just click [here](https://infinitejetpack.github.io/edison-SI) or go to https://infinitejetpack.github.io/edison-SI for more about edison.

##License
<a rel="license" href="http://creativecommons.org/licenses/by-nd/4.0/"><img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by-nd/4.0/88x31.png" /></a><br /><span xmlns:dct="http://purl.org/dc/terms/" property="dct:title">edison</span> by <a xmlns:cc="http://creativecommons.org/ns#" href="https://github.com/infinitejetpack" property="cc:attributionName" rel="cc:attributionURL">InfiniteJetpack</a> is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-nd/4.0/">Creative Commons Attribution-NoDerivatives 4.0 International License</a>.<br />Based on a work at <a xmlns:dct="http://purl.org/dc/terms/" href="https://github.com/InfiniteJetpack/edison-SI" rel="dct:source">https://github.com/InfiniteJetpack/edison-SI</a>.
