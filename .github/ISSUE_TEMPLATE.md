Hello GitHub Users,
I have identified a problem in edison's source code.

Here is the error I'm getting:

  Description:
    _Explain the issue here!_
    
  Image:
    _Insert screenshot(s) of your results here!_
    
Here are the steps I followed to get this error:

  1. _did this_
  2. _did that_
  3. _did these_
  4. _did those_
  
Here are my system specs:

edison version: _x.x-beta.x - if you are not using a RELEASE then PLEASE SPECIFY DATE OF DOWNLOAD!_

Operating system: _name e.g. macOS_ _version codename e.g. Yosemite_
OS version: _x.x.x_

Computer: _type e.g. MacBook_ _subtype e.g. Pro_
Computer Age: _early/late and a year_
