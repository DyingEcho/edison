Hello GitHub Users,

I have identified and fixed an issue in edison's source code.

The bug I have fixed is [Issue № *number*](*link* "Issue № *number*").

Could someone with appropriate privileges please review my pull request?

Thanks,

*your name*
