# How to contribute to edison
### Do you want to contribute to the edison project? If so, please read through the guidelines first. All help is appreciated.

### **EEK! Bugs!**
#### If you have found a bug, please complete the following steps:

* **Has someone already found it?** Do a search on Issues to make sure nobody has already identified the insect. 

* **Can't find any related issues?** File a new one! Please include:
  * A **title and desccription** that will help other people squish the bug;
  * The **error** you're getting;
  * The **code** you think is responsible.

* **You're good to go!** That bug will never know what hit it.

### **Did you squish a bug?**
#### Once you've finished with the bug spray, please do the following:

* **Start a pull request.**

* Make sure everything is **clear** and **anyone could understand** what you have fixed.

* Include the **issue number and title**, with a link to the issue.

* Submit! Thanks for helping kill roaches, beetles and creepy-crawlies!

### Do you want to add a new feature to edison?

* If you want to write some code and add a feature... what are you still here for? Get coding! After you're done please submit a pull request.

* If you want to request a new feature, please:
  
  * Go to the Issues page and create a new issue.
  
  * Give it a **relevant and clear title + description** as well as how you think it could be done.
  
  * Submit! People can comment on it, suggest improvements, and work on implementing it.

## We cannot thank you enough. No matter how you contribute, you have been a great help.
